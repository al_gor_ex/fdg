export enum LineType {
    Solid = 'solid',
    Dashed = 'dashed'
}