import { cartToScr } from "./coordinate-systems-functions.js";
import { GraphParameters } from "./graph-parameters.js";
import { Point } from "./point.js";
import { Vector } from "./vector.js";

export class Vertex {
    forces: Vector[] = []

    constructor(
        public id: number,
        public mass: number,
        public color: string,
        public label: string,
        public center: Point
    ) { }

    draw(canvas: HTMLCanvasElement, k: number, dX: number, dY: number, withLabel: boolean = true): void {
        let context = canvas.getContext('2d')
        context.beginPath()
        context.fillStyle = this.color
        let vertexCenter = cartToScr(this.center, canvas.height, k, dX, dY)
        context.arc(vertexCenter.x, vertexCenter.y, this.mass * GraphParameters.vertexSizeCoef * k, 0, 2 * Math.PI)
        context.fill()
        context.closePath()
        if (withLabel) {
            this.drawLabel(canvas, k, dX, dY)
        }
    }

    drawLabel(canvas: HTMLCanvasElement, k: number, dX: number, dY: number): void {
        let context = canvas.getContext('2d')
        let vertexCenter = cartToScr(this.center, canvas.height, k, dX, dY)
        context.font = '20px serif'
        context.fillStyle = 'black'
        context.fillText(this.label, vertexCenter.x, vertexCenter.y)
    }
}