export class Vector {
    constructor(
        public x: number,
        public y: number
    ) {}

    rotate(angle: number): void {
        let newX = Math.cos(angle) * this.x - Math.sin(angle) * this.y
        let newY = Math.sin(angle) * this.x + Math.cos(angle) * this.y
        this.x = newX
        this.y = newY
    }

    add(other: Vector): void {
        this.x += other.x
        this.y += other.y
    }

    divideByValue(divider: number): void {
        this.x /= divider
        this.y /= divider
    }

    getCopy(): Vector {
        return new Vector(this.x, this.y)
    }
}