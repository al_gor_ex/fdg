import { cartToScr, scrToCart } from "./coordinate-systems-functions.js"
import { Graph } from "./graph.js"
import { Point } from "./point.js"

export class Environment {
    canvas: HTMLCanvasElement
    mousePosition: Point
    // zoom
    k: number = 1
    // axes offset
    dX: number = 0
    dY: number = 0
    graph: Graph

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas
        this.graph = new Graph()
        this.mousePosition = new Point(0, 0)
    }

    makeAnimationLoop(): void {
        this.graph.recalculateForces(
            this.mousePosition,
            new Point(this.canvas.width / 2, this.canvas.height / 2)
        )
        this.graph.moveVertices()
        this.draw()
        window.requestAnimationFrame(this.makeAnimationLoop.bind(this))
    }

    updateMousePosition(x: number, y: number) {
        this.mousePosition = scrToCart(
            new Point(x, y),
            this.canvas.height,
            this.k,
            this.dX,
            this.dY
        )
    }

    draw(): void {
        this.fillBackground(this.canvas)
        this.drawAttractionCenterMark(this.canvas)
        this.graph.draw(
            this.canvas,
            this.mousePosition,
            this.k,
            this.dX,
            this.dY
        )
    }

    private fillBackground(canvas: HTMLCanvasElement): void {
        let context = canvas.getContext('2d')
        context.fillStyle = 'lightcyan'
        context.fillRect(0, 0, canvas.width, canvas.height)
    }

    private drawAttractionCenterMark(canvas: HTMLCanvasElement): void {
        let context = canvas.getContext('2d')
        let attrCenter = cartToScr(
            new Point(canvas.width / 2, canvas.height / 2),
            canvas.height,
            this.k,
            this.dX,
            this.dY
        )
        context.strokeStyle = 'magenta'
        context.beginPath()
        context.moveTo(attrCenter.x - 5, attrCenter.y)
        context.lineTo(attrCenter.x + 5, attrCenter.y)
        context.stroke()
        context.closePath()
        context.beginPath()
        context.moveTo(attrCenter.x, attrCenter.y - 5)
        context.lineTo(attrCenter.x, attrCenter.y + 5)
        context.stroke()
        context.closePath()
    }
}