import { Point } from "./point.js";

/**
 * Converts point's Cartesian coordinates into real coordinates on the canvas
 * @param h canvas height
 * @param k zoom coefficient
 * @param deltaX Y axis offset
 * @param deltaY X axis offset
 */
export function cartToScr(
    point: Point,
    h: number,
    k: number,
    deltaX: number,
    deltaY: number
): Point {
    return new Point(
        deltaX + k * point.x, 
        h - deltaY - k * point.y
    )
}

/**
 * Converts point's real coordinates on the canvas into Cartesian
 * @param h canvas height
 * @param k zoom coefficient
 * @param deltaX Y axis offset
 * @param deltaY X axis offset
 */
export function scrToCart(
    point: Point,
    h: number,
    k: number,
    deltaX: number,
    deltaY: number
): Point {
    return new Point(
        (point.x - deltaX) / k, 
        (h - deltaY - point.y) / k
    )
}