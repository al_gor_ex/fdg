import { scrToCart } from "./coordinate-systems-functions.js"
import { Environment } from "./environment.js"
import { GraphParameters } from "./graph-parameters.js"
import { LabelsVisibility } from "./labels-visibility.js"
import { LineType } from "./line-type.js"
import { Point } from "./point.js"

let environment: Environment

window.onload = () => onLoad()

function onLoad(): void {
    let canvas = document.getElementById('canvas') as HTMLCanvasElement
    environment = new Environment(canvas)
    canvas.onmousemove = (event: MouseEvent) => {
        environment.updateMousePosition(event.offsetX, event.offsetY)
    }
    canvas.onmousedown = () => {
        environment.graph.captureClosestVertex(environment.mousePosition)
    }
    canvas.onmouseup = () => {
        environment.graph.releaseCapturedVertex()
    }
    canvas.onclick = (event: MouseEvent) => {
        if (event.ctrlKey) {
            environment.graph.deleteClosestElement(
                scrToCart(
                    new Point(event.offsetX, event.offsetY),
                    canvas.height,
                    environment.k,
                    environment.dX,
                    environment.dY
                )
            )
            updateVerticesSelectLists()
        }
    }
    (document.getElementById('btn-add-vertex') as HTMLButtonElement).onclick = () => {
        environment.graph.addVertex(
            Number((document.getElementById('new-vertex-mass') as HTMLSelectElement).value),
            (document.getElementById('new-vertex-color') as HTMLInputElement).value,
            (document.getElementById('new-vertex-label') as HTMLInputElement).value,
            new Point(100, 100)
        )
        updateVerticesSelectLists()
    }
    (document.getElementById('btn-add-edge') as HTMLButtonElement).onclick = () => {
        environment.graph.addEdge(
            Number((document.getElementById('new-edge-vertex-1') as HTMLSelectElement).value),
            Number((document.getElementById('new-edge-vertex-2') as HTMLSelectElement).value),
            Number((document.getElementById('new-edge-length') as HTMLSelectElement).value),
            (document.getElementById('new-edge-color') as HTMLInputElement).value,
            (document.getElementById('new-edge-line-type') as HTMLInputElement).value as LineType,
            (document.getElementById('new-edge-label') as HTMLInputElement).value
        )
    }
    (document.getElementById('btn-zoom-in') as HTMLButtonElement).onclick = () => {
        environment.k *= 1.25
        document.getElementById('zoom-value').innerHTML = environment.k.toFixed(1)
    }
    (document.getElementById('btn-zoom-out') as HTMLButtonElement).onclick = () => {
        environment.k /= 1.25
        document.getElementById('zoom-value').innerHTML = environment.k.toFixed(1)
    }
    (document.getElementById('btn-move-up') as HTMLButtonElement).onclick = () => {
        environment.dY -= 50
    }
    (document.getElementById('btn-move-down') as HTMLButtonElement).onclick = () => {
        environment.dY += 50
    }
    (document.getElementById('btn-move-left') as HTMLButtonElement).onclick = () => {
        environment.dX += 50
    }
    (document.getElementById('btn-move-right') as HTMLButtonElement).onclick = () => {
        environment.dX -= 50
    }
    (document.getElementById('labels-visibility-always') as HTMLInputElement).onchange = () => {
        onLabelsVisibilityChanged()
    }
    (document.getElementById('labels-visibility-on-hover') as HTMLInputElement).onchange = () => {
        onLabelsVisibilityChanged()
    }
    (document.getElementById('is-freezed') as HTMLInputElement).onchange = (ev: Event) => {
        environment.graph.isFreezed = (ev.target as HTMLInputElement).checked
    }
    document.getElementById('increase-attraction').onclick = () => {
        GraphParameters.gAttr += 0.005
    }
    document.getElementById('descrease-attraction').onclick = () => {
        if (GraphParameters.gAttr > 0) {
            GraphParameters.gAttr -= 0.005
        }
    }
    environment.makeAnimationLoop()
}

function onLabelsVisibilityChanged(): void {
    let radios = document.getElementsByName('labels-visibility') as NodeListOf<HTMLInputElement>
    environment.graph.labelsVisibility = (radios[0].checked) ?
        LabelsVisibility.Always :
        LabelsVisibility.OnHover
}

function updateVerticesSelectLists(): void {
    let verticesSelect1 = document.getElementById('new-edge-vertex-1') as HTMLSelectElement
    let verticesSelect2 = document.getElementById('new-edge-vertex-2') as HTMLSelectElement
    for (let select of [verticesSelect1, verticesSelect2]) {
        select.innerHTML = ''
        for (let vertex of environment.graph.vertices) {
            select.innerHTML += `<option value="${vertex.id}">${vertex.label}</option>`
        }
    }
}