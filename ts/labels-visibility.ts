export enum LabelsVisibility {
    Always = 'always',
    OnHover = 'on-hover'
}