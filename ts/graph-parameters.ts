export class GraphParameters {
    /** Edge tension coefficient */ 
    static k: number = 0.75
    /** Number of pixels in edge length unit */
    static edgeLengthCoef: number = 50
    /** Coefficient of attraction of each vertex to the screen center */
    static gAttr: number = 0.01
    /** Coefficient of repulsement between every two vertices*/    
    static gRepul: number = 1000
    /** Coefficient of attraction of each vertex to the mouse cursor */
    static gamma: number = 0.005
    static vertexMassCoef: number = 2
    static vertexSizeCoef: number = 5
    /** Angle of arrow wings offset from edge's line */
    static arrowWidthAngle: number = Math.PI / 12
    /** Edge to its arrow length ratio */
    static arrowLengthCoef: number = 10
    /** Edge arrow will be moved back from the end of the edge. Edge's offset part will be equal to this coef */
    static arrowTipOffsetCoef: number = 1 / 4
}