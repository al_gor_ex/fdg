import { cartToScr } from "./coordinate-systems-functions.js";
import { GraphParameters } from "./graph-parameters.js";
import { LineType } from "./line-type.js";
import { Point } from "./point.js";
import { Vector } from "./vector.js";
import { Vertex } from "./vertex.js";

export class Edge {
    constructor(
        public length: number,
        public color: string,
        public lineType: LineType,
        public label: string,
        public vertex1: Vertex,
        public vertex2: Vertex
    ) { }

    draw(
        canvas: HTMLCanvasElement,
        k: number,
        deltaX: number,
        deltaY: number,
        withLabel: boolean = true
    ): void {
        let context = canvas.getContext('2d')
        context.beginPath()
        context.lineWidth = 2
        context.strokeStyle = this.color
        switch (this.lineType) {
            case LineType.Dashed:
                context.setLineDash([4, 2])
                break;
            case LineType.Solid:
                context.setLineDash([0, 0])
                break;
        }
        let vertex1Center = cartToScr(
            this.vertex1.center,
            canvas.height,
            k,
            deltaX,
            deltaY
        )
        context.moveTo(vertex1Center.x, vertex1Center.y)
        let vertex2Center = cartToScr(
            this.vertex2.center,
            canvas.height,
            k,
            deltaX,
            deltaY
        )
        context.lineTo(vertex2Center.x, vertex2Center.y)
        context.stroke()
        context.closePath()
        if (withLabel) {
            this.drawLabel(canvas, k, deltaX, deltaY)
        }
        this.drawArrow(canvas, k, deltaX, deltaY)
    }

    drawArrow(
        canvas: HTMLCanvasElement,
        k: number,
        deltaX: number,
        deltaY: number
    ): void {
        let arrowLength = this.vertex2.center.getDistanceFrom(this.vertex1.center)
        let arrowAngle = this.vertex1.center.getAngleTo(this.vertex2.center)
        
        // Find the arrow tip point (move it a bit backward from the edge end)
        let tipPoint = this.vertex2.center.getCopy()
        let tipOffsetVector = new Vector(arrowLength * GraphParameters.arrowTipOffsetCoef, 0)
        tipOffsetVector.rotate(arrowAngle + Math.PI)
        tipPoint.move(tipOffsetVector)

        // Calculate arrow triangle points
        let vector = new Vector(arrowLength, 0)
        vector.divideByValue(GraphParameters.arrowLengthCoef)
        vector.rotate(arrowAngle + Math.PI)
        let leftWingPoint = tipPoint.getCopy()
        vector.rotate(GraphParameters.arrowWidthAngle)
        leftWingPoint.move(vector)

        let rightWingPoint = tipPoint.getCopy()
        vector.rotate(-2 * GraphParameters.arrowWidthAngle)
        rightWingPoint.move(vector)

        // Convert them to screen coords
        tipPoint = cartToScr(new Point(tipPoint.x, tipPoint.y), canvas.height, k, deltaX, deltaY)
        leftWingPoint = cartToScr(new Point(leftWingPoint.x, leftWingPoint.y), canvas.height, k, deltaX, deltaY)
        rightWingPoint = cartToScr(new Point(rightWingPoint.x, rightWingPoint.y), canvas.height, k, deltaX, deltaY)

        // Draw
        let context = canvas.getContext('2d')
        context.fillStyle = this.color
        context.beginPath()
        context.moveTo(tipPoint.x, tipPoint.y)
        context.lineTo(leftWingPoint.x, leftWingPoint.y)
        context.lineTo(rightWingPoint.x, rightWingPoint.y)
        context.lineTo(tipPoint.x, tipPoint.y)
        context.fill()
        context.closePath()
    }

    drawLabel(
        canvas: HTMLCanvasElement,
        k: number,
        deltaX: number,
        deltaY: number
    ): void {
        let context = canvas.getContext('2d')
        let edgeCenter = cartToScr(
            this.getCenter(),
            canvas.height,
            k,
            deltaX,
            deltaY
        )
        context.font = '20px serif'
        context.fillStyle = 'black'
        context.fillText(this.label, edgeCenter.x, edgeCenter.y)
    }

    getCenter(): Point {
        return new Point(
            (this.vertex1.center.x + this.vertex2.center.x) / 2,
            (this.vertex1.center.y + this.vertex2.center.y) / 2
        )
    }
}