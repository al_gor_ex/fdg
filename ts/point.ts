import { Vector } from "./vector"

export class Point {
    constructor(
        public x: number,
        public y: number
    ) {}

    getDistanceFrom(other: Point): number {
        return Math.sqrt((this.x - other.x) ** 2 + (this.y - other.y) ** 2)
    }

    getAngleTo(other: Point): number {
        return Math.atan2((other.y - this.y), (other.x - this.x))
    }

    move(vector: Vector): void {
        this.x += vector.x
        this.y += vector.y
    }

    getCopy(): Point {
        return new Point(this.x, this.y)
    }
}