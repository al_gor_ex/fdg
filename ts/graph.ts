import { Edge } from "./edge.js"
import { GraphParameters } from "./graph-parameters.js"
import { LabelsVisibility } from "./labels-visibility.js"
import { LineType } from "./line-type.js"
import { Point } from "./point.js"
import { Vector } from "./vector.js"
import { Vertex } from "./vertex.js"

export class Graph {
    vertices: Vertex[] = []
    edges: Edge[] = []
    labelsVisibility: LabelsVisibility = LabelsVisibility.Always
    isFreezed: boolean = false
    capturedVertex: Vertex | null = null

    draw(canvas: HTMLCanvasElement, mousePosition: Point, k: number, dX: number, dY: number): void {
        let withLabels = (this.labelsVisibility == LabelsVisibility.Always)
        for (let edge of this.edges) {
            edge.draw(canvas, k, dX, dY, withLabels)
        }
        for (let vertex of this.vertices) {
            vertex.draw(canvas, k, dX, dY, withLabels)
        }
        if (this.labelsVisibility == LabelsVisibility.OnHover) {
            let closestElement = this.getClosestElement(mousePosition)
            closestElement.drawLabel(canvas, k, dX, dY)
        }
    }

    addVertex(
        mass: number,
        color: string,
        label: string,
        center: Point
    ): void {
        this.vertices.push(new Vertex(
            this.getIdForNewVertex(),
            mass,
            color,
            label,
            center
        ))
    }

    addEdge(
        vertex1Id: number,
        vertex2Id: number,
        length: number,
        color: string,
        lineType: LineType,
        label: string
    ): void {
        let edgeAlreadyExists = (this.edges.filter(
            e => e.vertex1.id == vertex1Id && e.vertex2.id == vertex2Id
        ).length > 0)
        if (edgeAlreadyExists) {
            alert('Выбранные вершины уже соединены ребром')
            return
        }
        let vertex1 = this.vertices.filter(v => v.id == vertex1Id)[0]
        let vertex2 = this.vertices.filter(v => v.id == vertex2Id)[0]
        this.edges.push(new Edge(
            length,
            color,
            lineType,
            label,
            vertex1,
            vertex2
        ))
    }

    captureClosestVertex(mousePosition: Point): void {
        let closestVertex = this.getClosestElement(mousePosition)
        if (closestVertex instanceof Vertex == false) {
            return
        }
        this.capturedVertex = closestVertex as Vertex
    }

    releaseCapturedVertex(): void {
        this.capturedVertex = null
    }

    deleteClosestElement(point: Point): void {
        let element = this.getClosestElement(point)
        if (element instanceof Edge) {
            this.deleteEdge(element)
        }
        if (element instanceof Vertex) {
            this.deleteVertex(element)
        }
    }

    recalculateForces(mousePosition: Point, screenCenter: Point): void {
        for (let vertex of this.vertices) {
            vertex.forces = []
        }
        this.recalculateRepulsementForces()
        this.recalculateEdgeTensionForces()
        this.recalculateCenterAttractionForces(screenCenter)
        this.recalculateMouseAttractionForce(mousePosition)
    }

    moveVertices(): void {
        if (this.isFreezed) {
            return
        }
        for (let vertex of this.vertices) {
            let forcesSum = new Vector(0, 0)
            for (let force of vertex.forces) {
                forcesSum.add(force)
            }
            let velocity = forcesSum.getCopy()
            velocity.divideByValue(vertex.mass * GraphParameters.vertexMassCoef)
            vertex.center.move(velocity)
        }
    }

    private deleteEdge(edge: Edge): void {
        this.edges = this.edges.filter(e => !(e.vertex1 == edge.vertex1 && e.vertex2 == edge.vertex2))
    }

    private deleteVertex(vertex: Vertex): void {
        let incidentEdges = this.edges.filter(e => (e.vertex1.id == vertex.id || e.vertex2.id == vertex.id))
        for (let edge of incidentEdges) {
            this.deleteEdge(edge)
        }
        this.vertices = this.vertices.filter(v => v.id !== vertex.id)
    }

    private recalculateRepulsementForces(): void {
        for (let i = 0; i < this.vertices.length; i++) {
            for (let j = i + 1; j < this.vertices.length; j++) {
                if (i == j) {
                    continue
                }
                let ver1 = this.vertices[i]
                let ver2 = this.vertices[j]
                let distance = ver1.center.getDistanceFrom(ver2.center)
                let forceVectorValue =
                    GraphParameters.gRepul *
                    ver1.mass * ver2.mass * GraphParameters.vertexMassCoef ** 2 /
                    distance ** 2
                let vector1 = new Vector(forceVectorValue, 0)
                let angle = ver1.center.getAngleTo(ver2.center)
                vector1.rotate(angle + Math.PI)
                ver1.forces.push(vector1)
                let vector2 = vector1.getCopy()
                vector2.rotate(Math.PI)
                ver2.forces.push(vector2)
            }
        }
    }

    private recalculateEdgeTensionForces(): void {
        for (let edge of this.edges) {
            let ver1 = edge.vertex1
            let ver2 = edge.vertex2
            let realEdgeLength = ver1.center.getDistanceFrom(ver2.center)
            let normalEdgeLength = edge.length * GraphParameters.edgeLengthCoef
            let forceValue = GraphParameters.k * (realEdgeLength - normalEdgeLength)
            let angle = ver1.center.getAngleTo(ver2.center)
            let vector1 = new Vector(forceValue, 0)
            vector1.rotate(angle)
            ver1.forces.push(vector1)
            let vector2 = vector1.getCopy()
            vector2.rotate(Math.PI)
            ver2.forces.push(vector2)
        }
    }

    private recalculateCenterAttractionForces(screenCenter: Point): void {
        for (let vertex of this.vertices) {
            let forceValue = GraphParameters.gAttr * vertex.center.getDistanceFrom(screenCenter)
            let angle = vertex.center.getAngleTo(screenCenter)
            let vector = new Vector(forceValue, 0)
            vector.rotate(angle)
            vertex.forces.push(vector)
        }
    }

    private recalculateMouseAttractionForce(mouseCoords: Point): void {
        if (this.capturedVertex !== null) {
            let forceValue = GraphParameters.gamma *
                this.capturedVertex.center.getDistanceFrom(mouseCoords) ** 2
            let angle = this.capturedVertex.center.getAngleTo(mouseCoords)
            let vector = new Vector(forceValue, 0)
            vector.rotate(angle)
            this.capturedVertex.forces.push(vector)
        }
    }

    private getClosestElement(point: Point): Vertex | Edge {
        let closestElement: Vertex | Edge = this.vertices[0]
        let minDistance = this.vertices[0].center.getDistanceFrom(point)
        for (let vertex of this.vertices) {
            let distance = vertex.center.getDistanceFrom(point)
            if (distance < minDistance) {
                minDistance = distance
                closestElement = vertex
            }
        }
        for (let edge of this.edges) {
            let distance = edge.getCenter().getDistanceFrom(point)
            if (distance < minDistance) {
                minDistance = distance
                closestElement = edge
            }
        }
        return closestElement
    }

    private getIdForNewVertex(): number {
        let id = 0
        // Get the max id through all vertices
        for (let vertex of this.vertices) {
            if (vertex.id > id) {
                id = vertex.id
            }
        }
        return id + 1
    }
}