# Force-directed graph implementation

## Теория

Силовая визуализация графов — алгоритм визуализации графа в виде, удобном для просмотра человеком. Он помогает расположить узлы графа в пространстве так, чтобы вершины были достаточно удалены друг от друга, и число пересечений рёбер было минимально, путём назначения сил вершинам и узлам графа.

Текущая реализация использует законы Кулона (представляя все вершины как одноимённо заряженные частицы) и Гука (рёбра графа являются упругими нитями). Дополнительно добавлены силы притяжения к центру для всех вершин (граф должен стремиться оставаться как можно ближе к центру экрана) и сила притяжения вершины к курсору мыши (действует только на захваченную вершину).

![](https://sun9-18.userapi.com/impf/0KAkzgo-IcN3UlLmukkzcvdC2NDzBJT98sxQnQ/oJhQNTgEbmE.jpg?size=538x473&quality=96&sign=4ce619b1ab2da427510da8197f732a0f&type=album)

![](https://sun1-98.userapi.com/impf/ibSliAdw1Kqvs9mXiLTWc_ACtNqNhkaJ-W7wsQ/ERiBMWsB5SA.jpg?size=582x370&quality=96&sign=2243081a86f1563ad3078e19f6292631&type=album)

## Функциональность

Приложение позволяет:

- создавать вершины различной массы и цвета, задавая им подписи
- соединять их подписанными рёбрами различной длины, цвета и начертания
- перетаскивать вершины мышью
- удалять вершины и рёбра
- "замораживать" граф на месте, отключая действие всех сил

## Установка

Перед запуском приложения необходимо скомпилировать TypeScript-файлы, вызвав в корневом каталоге проекта команду:

`tsc --project tsconfig.json`

после этого можно обращаться к файлу `index.html`

## Видео

[доступно по ссылке](https://vk.com/doc349378999_629248151?hash=9ba5141d26c9be2ea6&dl=9eff7c6c61c5c5ea0b&wnd=1&module=im&mp4=1)

## Скриншоты

![](https://sun9-56.userapi.com/impf/cawFzGna2zbH6-cUeBbuIFsWOwefOK4VEmIuRQ/l5Gz5bjy-SE.jpg?size=1196x586&quality=95&sign=871ddc0fd57eecd25cbd74dccca4831e&type=album)

![](https://sun9-88.userapi.com/impf/2tFKXKKe-Ewvsmg6hkxR7gIKEQN6W016ZIPAzg/FbaGdt0BW70.jpg?size=1189x598&quality=95&sign=490558fcfd806ba4a903061e5f047455&type=album)

![](https://sun9-20.userapi.com/impf/wk40L6SekS9fBMr6R0d-qNYF9pQaydqfzeXMmg/V5l0hfKguNQ.jpg?size=1189x578&quality=95&sign=1dbd3348882c45d4387f7e5c3f930bef&type=album)

![](https://sun9-34.userapi.com/impf/qKYX9rMx2HVcoPfMH38gfxFYfo1gguPHq7W_Nw/WGP6pS0H8oI.jpg?size=1301x587&quality=95&sign=c0acbd863742f126cb4da6e7362cd8f2&type=album)

![](https://sun9-3.userapi.com/impf/mM_Szercj6hbM6xkSQj4Kk9aPctfmTh3xkEY7Q/gFKzlknKh_0.jpg?size=1196x588&quality=95&sign=d7b385c37aecd157b9268cc40303417c&type=album)
